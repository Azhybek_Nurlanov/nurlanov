/**
 * Updated to new version of formstone.js background
 * which have new structure and new dependencies
 * such as core.js , transition.js and background.js
 * since v1.3.9
 * jquery.fs.wallpaper.js, jquery.fs.wallpaper.min.js and scripts.js are @deprecated
 * it remains for backward compatibility.
 */
jQuery(document).ready(function($) {
	$('.background-video').background();
});

/*This file was exported by "Export WP Page to Static HTML" plugin which created by ReCorp (https://myrecorp.com) */